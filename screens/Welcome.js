import React, { useState, useEffect, useCallback } from "react";
import {
  Text,
  View,
  Image,
  ImageBackground,
  TouchableOpacity,
  Platform,
  Linking,
  Alert,
} from "react-native";
//component = function
//create a variable which reference to a function
import { sum2Number, substract2Number, PI } from "../utilies/Calculation";
import { images, icons, colors, fontSizes } from "../constants";
import { UIButton } from "../components";
import Icon from "react-native-vector-icons/FontAwesome5";
import AsyncStorage from "@react-native-async-storage/async-storage";

import {
  auth,
  onAuthStateChanged,
  firebaseDatabaseRef,
  firebaseSet,
  firebaseDatabase,
  onValue,
} from "../firebase/firebase";
import FakeView from "./FakeView";

const OpenURLButton = ({ url, children }) => {
  console.log(url);
  const handlePress = useCallback(async () => {
    Linking.openURL(url);
  }, [url]);

  return (
    <UIButton
      title={children}
      // isSelected={accountType.isSelected}
      onPress={handlePress}
    />
  );
};

function Welcome(props) {
  //state => when a state is changed => UI is reloaded
  //like getter/setter
  const [accountTypes, setAccountTypes] = useState([
    {
      id: 1,
      name: "Đăng ký",
      isSelected: true,
    },
    {
      id: 2,
      name: "Đăng nhập",
      isSelected: false,
    },
    {
      id: 3,
      name: "Liên hệ CSKH",
      isSelected: false,
    },
  ]);
  const [linkSignup, setlinkSignup] = useState("");
  const [linkTele, setlinkTele] = useState("");
  const [authen, setAuthen] = useState(false);
  const [loading, setLoading] = useState(true);
  //navigation
  const { navigation, route } = props;
  // functions of navigate to/back
  const { navigate, goBack } = navigation;
  useEffect(() => {
    onValue(firebaseDatabaseRef(firebaseDatabase, "show"), async (snapshot) => {
      setLoading(false);
      if (snapshot.exists()) {
        let snapshotObject = snapshot.val();
        setlinkSignup(snapshotObject.hi88);
        setlinkTele(snapshotObject.telegram);
        setAuthen(snapshotObject.authen);
      } else {
        console.log("No data available");
      }
    });
  }, []);
  useEffect(() => {
    onAuthStateChanged(auth, (responseUser) => {
      // debugger
      if (responseUser) {
        //save data to Firebase
        let user = {
          userId: responseUser.uid,
          email: responseUser.email,
          emailVerified: responseUser.emailVerified,
          accessToken: responseUser.accessToken,
        };
        firebaseSet(
          firebaseDatabaseRef(firebaseDatabase, `users/${responseUser.uid}`),
          user
        );
        //save user to local storage
        AsyncStorage.setItem("user", JSON.stringify(user));
        navigate("UITab");
      }
    });
  });
  return loading ? (
    <View />
  ) : authen ? (
    <View
      style={{
        backgroundColor: "white",
        flex: 100,
      }}
    >
      <ImageBackground
        source={images.background}
        resizeMode="cover"
        style={{
          flex: 100,
        }}
      >
        <View
          style={{
            flex: 20,
          }}
        >
          <View
            style={{
              flexDirection: "row",
              height: 50,
              justifyContent: "flex-start",
              alignItems: "center",
              marginTop: Platform.OS === "ios" ? 40 : 0,
            }}
          >
            <View style={{ flex: 1 }} />
            <Icon
              name={"question-circle"}
              color={"white"}
              size={20}
              style={{
                marginEnd: 20,
              }}
            />
          </View>
        </View>
        <View
          style={{
            flex: 20,
            width: "100%",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Text
            style={{
              marginBottom: 7,
              color: "white",
              fontSize: fontSizes.h6,
            }}
          >
            Welcome to
          </Text>
          <Text
            style={{
              textTransform: "uppercase",
              marginBottom: 7,
              color: "white",
              fontWeight: "bold",
              fontSize: fontSizes.h3,
            }}
          >
            Hi88!
          </Text>
          {/* <Text style={{
                    marginBottom: 7,
                    color: 'white',
                    fontSize: fontSizes.h6,
                }}>Please select your account type</Text> */}
        </View>
        <View
          style={{
            flex: 40,
          }}
        >
          <OpenURLButton url={linkSignup}>Đăng ký</OpenURLButton>
          <OpenURLButton url={linkSignup}>Đăng nhập</OpenURLButton>
          <OpenURLButton url={linkTele}>Liên hệ CSKH</OpenURLButton>
        </View>
        <View
          style={{
            flex: 20,
          }}
        >
          <UIButton
            onPress={() => {
              navigate("Login");
            }}
            title={"Login".toUpperCase()}
          />
          <Text
            style={{
              color: "white",
              fontSize: fontSizes.h6,
              alignSelf: "center",
            }}
          >
            Want to register new Account ?
          </Text>
          <TouchableOpacity
            onPress={() => {
              // alert('press register')
              navigate("Register");
            }}
            style={{
              padding: 5,
            }}
          >
            <Text
              style={{
                color: colors.primary,
                fontSize: fontSizes.h6,
                alignSelf: "center",
                textDecorationLine: "underline",
              }}
            >
              Register
            </Text>
          </TouchableOpacity>
        </View>
      </ImageBackground>
    </View>
  ) : (
    <FakeView />
  );
}
export default Welcome;
