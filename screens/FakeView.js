import { View, Text, FlatList, Image } from "react-native";
import React from "react";

const products = [
  {
    id: 1,
    name: "Áo Khoác Jean Hoodie Basic Blue Wash",
    image:
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ9j9X3s7egIliO5ko6QQoHgb7dnnVSHNOs27u3NNCnjA&s",
    size: "S",
    price: "20,000",
  },
  {
    id: 2,
    name: "Áo Khoác Jean Hoodie Basic Blue Wash",
    image:
      "https://product.hstatic.net/1000026602/product/dsc00977_3410829796fe4a3faca08bcd543a7b3c.jpg",
    size: "S",
    price: "20,000",
  },
  {
    id: 3,
    name: "Áo Khoác Jean Hoodie Basic Blue Wash",
    image:
      "https://product.hstatic.net/1000026602/product/dsc00995_139f9df88301481e95e8b82f5a9d2426.jpg",
    size: "S",
    price: "20,000",
  },
  {
    id: 4,
    name: "Áo Khoác Jean Hoodie Basic Blue Wash",
    image:
      "https://product.hstatic.net/1000026602/product/dsc07255_2337ba1123a545e7a9a016c8e7f5e3a4.jpg",
    size: "S",
    price: "20,000",
  },
  {
    id: 5,
    name: "Áo Khoác Jean Hoodie Basic Blue Wash",
    image:
      "https://product.hstatic.net/1000026602/product/dsc07550_8bdfad03972741b4a2cbd8a19ab3b4e5.jpg",
    size: "S",
    price: "20,000",
  },
  {
    id: 6,
    name: "Áo Khoác Jean Hoodie Basic Blue Wash",
    image:
      "https://product.hstatic.net/1000026602/product/dsc05760_4fb2321687de41aeb0ce6d3a205cd156.jpg",
    size: "S",
    price: "20,000",
  },
];

const FakeView = () => {
  const _renderItem = ({ item }) => {
    return (
      <View
        style={{
          width: "49%",
          alignItems: "center",
        }}
      >
        <Image
          source={{ uri: item.image }}
          style={{ width: 150, height: 200 }}
        />
        <Text>{item.name}</Text>
      </View>
    );
  };
  const _renderHeader = () => {
    return (
      <View>
        <Text
          style={{
            textAlign: "center",
            marginTop: 60,
            fontSize: 16,
            fontWeight: "600",
            color: "gray",
          }}
        >
          ÁO KHOÁC CHO NAM
        </Text>
      </View>
    );
  };
  return (
    <FlatList
      data={products}
      renderItem={_renderItem}
      keyExtractor={(item) => item.id.toString()}
      numColumns={2}
      style={{
        backgroundColor: "white",
        flex: 1,
        paddingHorizontal: 20,
      }}
      ListHeaderComponent={_renderHeader}
    />
  );
};

export default FakeView;
