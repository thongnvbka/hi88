import { initializeApp } from 'firebase/app'
import { 
    getAuth, 
    onAuthStateChanged,
    createUserWithEmailAndPassword,   
    signInWithEmailAndPassword,
    sendEmailVerification,  
    //read data from Firebase    
} from "firebase/auth"
//ref = reference to a "collection"
import { 
    getDatabase, 
    ref as firebaseDatabaseRef, 
    set as firebaseSet,
    child,
    get,
    onValue,
} from "firebase/database"

// import firestore from '@react-native-firebase/firestore';

const firebaseConfig = {
    apiKey: "AIzaSyCnt93waTxggvSxvdh4GmzyENMkiStGPEY",
    authDomain: "hi88-9b439.firebaseapp.com",
    databaseURL: "https://hi88-9b439-default-rtdb.asia-southeast1.firebasedatabase.app/",
    projectId: "hi88-9b439",
    storageBucket: "hi88-9b439.appspot.com",
    appId: '1:805280748008:android:efb8ad48a6aa19e376945d',
    messagingSenderId: "1032082090705",
}  
const app = initializeApp(firebaseConfig)
const auth = getAuth()
const firebaseDatabase = getDatabase()
export {
    auth,
    firebaseDatabase,
    createUserWithEmailAndPassword,
    onAuthStateChanged,
    firebaseSet,
    firebaseDatabaseRef,
    sendEmailVerification,
    child,
    get,
    onValue, //reload when online DB changed
    signInWithEmailAndPassword,
    // firestore
}

